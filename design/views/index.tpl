<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>{$page_title}</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
</head>
<body>
   <!-- content -->
   {$content}
   <!-- content  END-->
</body>
</html>
