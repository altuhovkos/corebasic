<?php

class MainController extends BaseController
{
	public function fetch()
	{
		$this->pageTitle = 'Главная';

		$this->view = 'main';

		$this->layout = 'index';

		$text = 'Hello World';

		$this->design->assign('text', $text);
	}
}