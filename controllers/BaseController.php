<?php

/**
 * AltuhovKernel
 *
 * @copyright 	2015 Altuhov Konstantin
 * @author 		Altuhov Konstantin
 *
 * Базовый класс для отображения страници, все новые контроллеры
 * должны его екстендить.
 *
 */

class BaseController
{

	//Натройка для доступа к сайту.
	//Если установлен TRUE, тогда что бы попасть на сайт,
	//пользователь будет направлен на ту страницу которую
	//укажем и на ней необходимо будет авторизироваться в системе.
	//Пока пользыватель не получет $_SESSION['auth'] = true; он будет
	//направлен на ту страницу которую мы указали. Не забудте создать
	//контроллер для данной страници.

	public $auth_mod = array(
								'status' => FALSE,

								//Страница но которую направляем пользователя если
								//он не авторизировался

								'redirect_to' => ''
							);

	//Заголовок страници

	public $pageTitle;

	//Подключаемый Layout. Внутри шаблона ?.tpl должен быть
	//указан {$content} для отображения содержимого страници
	//подключаемой с view

	public $layout = 'index';

	//Подключаемая View

	public $view = false;

	//Экземпляр класса Smarty

	public $design;

	//Конструктор настраивает Smarty

	function __construct() {
		core::authMod($this->auth_mod['status'], $this->auth_mod['redirect_to']);
		$this->design = new Smarty;
		$this->design->setCompileDir(core::app('config')->GetConfig('compiledDir'));
		$this->design->template_dir = $_SERVER['DOCUMENT_ROOT'].'/design/views/';
	}

	//Функция отображения страници

	public function render() {
		$path = core::app('config')->GetConfig('tpl_folder');
		$this->design->assign('content', $this->design->fetch($path.$this->view.'.tpl'));
		$this->design->assign('page_title', $this->pageTitle ? $this->pageTitle : core::app('config')->GetConfig('siteName'));
		$this->design->display($path.$this->layout.'.tpl');
	}

	//Функция для передачи в шаблон переменных

	public function assignVars($vars){
		foreach ($vars as $key => $value)
			$this->design->assign($key, $value);
	}

}
