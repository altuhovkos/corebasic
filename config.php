<?php

class Config
{

	public static function GetConfig($key = false)
	{
		$config = array(

			//DATABASE CONFIG

			'db' => array(
				'host' => 'localhost',
				'login' => 'root',
				'password' => '123456',
				'database' => 'redmine'
				),

			//DESIGN TPLs FOLDER

			'tpl_folder' => 'design/views/',

			//SITENAME

			'siteName' => 'Web App',

			//COMPILED DIR

			'compiledDir' => 'compiled'

		);

		return $key ? $config[$key] : $config;
	}
}

?>