<?php

/**
 * AltuhovKernel
 *
 * @copyright 	2015 Altuhov Konstantin
 * @author 		Altuhov Konstantin
 *
 * Класс ядра, содержит базовые методы
 * для выполнения скрипта.
 *
 */

class Core

{
	//Список моделей, при добавлениии модели указать название без разшерения

	public $classes = array(
					'db',
					'files'
				);

	//Статичная переменная, содержит екземпляр класса
	//что б не записывать его несколько раз

	public static $class = array();


	//Переменная  содержит обьект Smarty шаблонизатора

	public static $design;


	//Инициализация ядра

	function __construct() {

		$this->initKernel();

	}

	//Метод возвращает экземпляр указаного класса,
	//в $class указать навзание класса

	public static function app($class){

		if (!isset(self::$class[$class]))
			self::$class[$class] = new $class;

		return self::$class[$class];

	}

	//Функция редиректа. $url - страница

	public function redirect($url, $crush = false, $message = '') {

		header("location: $url");

		if ($crush)
			$this->crush($message);


	}

	//Функция перехватчик $_POST данных

	public function post($key = false, $default = false) {

		if (!$key) {
			return $_POST;
		}

		if (isset($_POST[$key]) && !empty($_POST[$key]))
			return $_POST[$key];
		else
			return $default;

	}

	//Функция перехватчик $_POST данных

	public function get($key = false, $default = false) {

		if (!$key) {
			return $_GET;
		}

		if (isset($_GET[$key]) && !empty($_GET[$key]))
			return $_GET[$key];
		else
			return $default;

	}

	public function server($key = false){

		if ($key)
			return $_SERVER[$key];
		else
			return $_SERVER;

	}

	public function session($key = false){

		if ($key)
			return $_SESSION[$key];
		else
			return $_SESSION;

	}

	public function crush($message = false){
		die($message);
	}

	//Функция инициализирует роботу скрипта


	public function initKernel(){

		$this->kernelSettings();

		$this->includes();

		$this->design = new Smarty;

		$this->render();

	}

	//Функция для мода авторизации

	public function authMod($status, $redirect_to){

		if ($status && !self::session('auth') && self::server('REQUEST_URI') != $redirect_to) {

			self::redirect($this->auth_mod['redirect_to']);

			$message = "<p>Если вы видите данное сообщение, ваш браузер не поддерживает автоматическое перенаправление</p><br>";

			$message.= "<a href='".$this->auth_mod['redirect_to']."'>На страницу авторизации</a>";

			self::crush($message);

		}

	}



	//Функция подключает модели, и необходимые скрипты

	public function includes(){

		foreach ($this->classes as $value)
			require_once $value.'.php';

		require_once './controllers/BaseController.php';

		require_once $this->server('DOCUMENT_ROOT').'/libs/smarty/libs/Smarty.class.php';

		require_once $this->server('DOCUMENT_ROOT').'/config.php';

	}

	//Функция возвращает нужный подключаеммый контроллер по URL, и
	//добавляет в $_GET данные после / в URL

	public function getUriData(){

		$uri = explode('/', $this->server('REQUEST_URI'));

		if (!empty($uri[1]))
			$controller_name = ucfirst($uri[1]).'Controller';
		else
			$controller_name = 'MainController';

		if (!file_exists('controllers/'.$controller_name.'.php')) {
			$this->redirect('/');
			exit;
		}

		unset($uri[0], $uri[1]);

		$request_get = array_chunk($uri, 2);

		foreach ($request_get as $value)
			$_GET[$value[0]] = $value[1];

		return $controller_name;

	}

	//Функция отображения и подключения нужного контроллера
	//$ctrl - имя контроллера, к примеру "MainController"

	public function render(){

		$ctrl = $this->getUriData();

		include_once 'controllers/'.$ctrl.'.php';

		$controller = new $ctrl;

		$controller->fetch();

		$controller->render();

	}

	//Настройки скрипта

	public function kernelSettings(){

		ini_set('display_errors', 1);

		error_reporting(E_ERROR | E_WARNING | E_PARSE);

		session_start();

		header('Content-Type: text/html; charset=utf-8');

	}

	//Функция фозвращает sql лимит. $page_val - номер страници, $limit_val - количество записей на страницу

	public function getLimits($page_val, $limit_val){

		return "LIMIT ".(max(1, intval($page_val))-1)*max(1, intval($limit_val)).", ".max(1, intval($limit_val));

	}

}