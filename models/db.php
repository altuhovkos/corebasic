<?php

/**
 * AltuhovKernel
 *
 * @copyright   2011 Altuhov Konstantin
 * @author      Altuhov Konstantin
 *
 * Класс для роботы с БД
 *
 */

class db
{
    function __construct() {
        $this->connectDb();
    }

    //Получить одну строку результата SQL запроса

	public function getSqlResult($sql){
        return mysql_fetch_assoc(mysql_query($sql));
    }

    //Получить все данные результата SQL запроса

    public function getSqlResults($sql){
        $query = mysql_query($sql);
        $result = false;
        while ($set = mysql_fetch_assoc($query)) $result[] = $set;
        return $result;
    }

    //Выполнить SQL запроса. Если указать
    //$return_id = TRUE возвратит последний записаный ID

    public function perfomSql($sql, $return_id = false){
        if ($return_id) {
            if (mysql_query($sql))
                return mysql_insert_id();
            else
                return false;
        }else
            if (mysql_query($sql)) return true; else return false;


    }

    //Функция устанавливает соиденение с БД

    public function connectDb(){
    	$config = config::GetConfig();
        mysql_connect($config['db']['host'], $config['db']['login'], $config['db']['password']);
        mysql_select_db($config['db']['database']);
        mysql_set_charset('utf8');
    }
}

?>